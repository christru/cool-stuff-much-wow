import json
import falcon


#I dont program python
def crossdomain(req, resp):
    resp.set_header('Access-Control-Allow-Origin', '*')

class UsersResource:

    #some mock users.
    users_fixture = json.dumps([{"id": 1, "name": "Chris"}, {"id": 2, "name": "Marnee"}, {"id": 3, "name": "Julian"}, {"id": 4, "name": "Frank"}])

    #return valid response, can probably check for id and pull one or collection of items.
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.body = UsersResource.users_fixture

    def on_post(self, req, resp):
        #try parsing json?
        try:
            data = req.stream.read()
            json_object = json.loads(data)
        #some big bad error
        except ValueError, e:
            raise falcon.HTTPBadRequest('Bad Request', str(e))
        else:
            resp.status = falcon.HTTP_201
            #return path to user

    #update find some user and update it.

    #delete some user.


app = falcon.API(after=[crossdomain])

users = UsersResource()

app.add_route('/users', users)
